const fs = require("fs");
const http = require("http");
const { Server } = require("socket.io");

const httpServer = http.createServer();
const io = new Server(httpServer);

httpServer.on("request", (req, res) => {
  if (req.url === "/") {
    const fileContent = fs.readFileSync("exampleSocketIo.html");
    res.end(fileContent.toString());
  }
});

const chatNamespace = io.of("chat");

const socketMap = {};

chatNamespace.on("connection", (socket) => {
  console.log("client connected! id: " + socket.id);

  socket.on("join room", (data) => {
    socket.join(data.room);
    socketMap[socket.id] = data;
    socket.to(data.room).emit("new user", data.username);
  });

  socket.on("new message", (data) => {
    const { username, message } = data;
    const currentData = socketMap[socket.id];
    socket.to(currentData.room).emit("new message", { username, message });
  });

  socket.on("disconnect", () => {
    const currentData = socketMap[socket.id];
    socket.to(currentData.room).emit("user left", currentData.username);
  });
});

httpServer.listen(8080, () => {
  console.log("HTTP Server is running on port 8080.");
});
