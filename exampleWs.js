const fs = require("fs");
const http = require("http");
const { WebSocketServer } = require("ws");

const server = http.createServer();
const wss = new WebSocketServer({ server });

server.on("request", (req, res) => {
  const fileContent = fs.readFileSync("exampleWs.html");
  res.end(fileContent.toString());
});

const rooms = {
  "qonaq otagi": [],
  "yataq otagi": [],
};

wss.on("connection", (socket) => {
  console.log("client connected!");

  socket.on("message", (data) => {
    console.log("message received from client " + data);

    wss.clients.forEach((client) => {
      if (client !== socket) {
        client.send("Ve aleykum salam!");
      }
    });
  });
});

server.listen(8080, () => {
  console.log("HTTP Server is running on port 8080.");
});
